from django.db import models
from django.utils import timezone


# Create your models here.
# Aqui cada clase es un modelo, normalmente.

class Post(models.Model):
    # Supuesto vinculo a otro modelo.
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)  # Titulo del POST.
    text = models.TextField()   # Contenido del POST
    create_date = models.DateTimeField(default=timezone.now)
    publish_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.publish_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title
